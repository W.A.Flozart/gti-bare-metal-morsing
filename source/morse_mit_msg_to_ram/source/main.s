.data
	message:	.asciz "nachricht"			@ string to morse (with trailing zero byte)
				.balign 4
	morse_code:	.space 64						@ create space for morse_code in memory
				.balign 4
  	morse_len:	.word 0x00						@ init morse_len with 0 (gets incremented in the append_ branches inside ascii_to_morse)
				.balign 4						

.section .init
.globl _start

.equ STACK_P, 0x80000
.equ BASE, 0x3f200000 		@Base address
.equ TIMER, 0x3f003004 		@Timer CLO address
.equ GPFSEL2, 0x08			@FSEL2 register offset 
.equ GPSET0, 0x1c			@GPSET0 register offset
.equ GPCLR0,0x28			@GPCLR0 register offset
.equ SET_BIT3,  0x08		@sets bit three b1000		
.equ SET_BIT21, 0x200000 	@sets bit 21

_start:

b main											@ Branch to the actual main code.

.section .text

main:

mov sp, #STACK_P								@ Set the stack pointer to 0x8000.

ldr		r3, =message							@ load address of string to r3
mov	  	r4, #0									@ write 0 to r4 to compare whether zero byte of string (message in r3) is reached

writeMessageToMemoryLoop:   
		ldrb	r8, [r3],#1						@ load char as hex to r8 by postfix address mode / load R3 to word addressed by r8. Increment R3 by 1.
		cmp   	r8,r4							@ check if last byte (zero byte) of string is reached (asciz appends zero byte)
		beq   	_morse							@ if zero byte is reached (end of string) jump to _morse
		bl 	 	ascii_to_morse					@ call ascii_to_morse functions if zero byte is not yet reached
		b    	writeMessageToMemoryLoop        @ loop over
		
_morse:
		bl		print_to_led					@ morse message from memory to led
		b 		_exit							@ end program


print_to_led:
		push 	{lr}

		ldr 	r3, =morse_code					@ get morse code from memory
		ldr 	r4, =morse_len					@ get address of morsecode in memory 
		ldr     r4, [r4]						@ get length of morsecode in memory

		morse_loop:
				ldrb		r8, [r3], #1		@ get next character by postfix address mode / load R3 to word addressed by r8. Increment R3 by 1.
		
		check_stop:
				cmp	 		r8, #0x2e @.
		  		bne	 		check_dash
		  		bl 	 		turn_on
		  		bl	 		time1
		  		bl	 		turn_off
				b			next_char

		check_dash:
				cmp	 		r8, #0x2d @-
				bne	 		check_space
				bl	 		turn_on
				bl	 		time3
				bl	 		turn_off
				b			next_char

		check_space:
				cmp	 		r8, #0x20 @' ' 
				bne	 		check_pipe
				bl	 		turn_off		  
				bl	 		time1
				b			next_char

		check_pipe:
				cmp	 		r8, #0x7c @|
				bne	 		next_char
				bl	 		turn_off
				bl	 		time3
				bl	 		time1


		next_char:
		  		bl			time1

		sub 	r4, #1
		cmp		r4, #0
		bne		morse_loop

		pop    {pc} 


GetGpioAddress: 
		ldr		r0,=BASE
		mov		pc,lr

SetGpioFunction:
		push 	{lr}
		ldr 	r1,=SET_BIT3
		str 	r1,[r0,#GPFSEL2]
		ldrb	r1,=SET_BIT21
		pop 	{pc}

turn_on:	
		push 	{r3,lr} 
		bl 		GetGpioAddress
		bl 		SetGpioFunction
		str 	r1,[r0,#GPSET0] @ turn led on

		pop 	{r3,pc}

turn_off:	
		push 	{r3,lr}
		bl 		GetGpioAddress
		bl 		SetGpioFunction
		str 	r1,[r0,#GPCLR0] @ turn led off

		pop 	{r3,pc}

GetTimeStamp:
		push 	{lr}
		ldr 	r0,=TIMER
		ldm 	r0, {r4,r5}
		pop 	{pc}

Wait:
		push	{lr}
		bl 		GetTimeStamp 				@ put lower 2 bytes of initial TimeStamp into r4
		mov 	r3,r4						@ pass TimeStamp value to r3

		timerloop:
				bl		GetTimeStamp		@ retrieve new TimeStamp corresponding to elapsed time
				sub 	r1,r4,r3			@ r1 = newest TimeStamp (retrieved in last line)  - inital TimeStamp
				cmp 	r1,r2				@ compare r2 (delay time in ms) to r1 (difference of r0(newest timestamp) and r3 (inital timestamp))
											@ set flag according to r1 - r2 

				bls 	timerloop			@ repeat loop if r1 - r2 is greater than zero

		pop {pc}


time1:
		ldr 	r0,=200000					@ delay time in microseconds
		mov 	r2,r0						@ pass delay in microsends to r2
		push 	{r0,r3,r4,r5,lr}
		bl 		Wait
		pop 	{r0,r3,r4,r5,pc}

time3:
		ldr 	r0,=600000					@ delay time in microseconds
		mov 	r2,r0						@ pass delay in microsends to r2
		push 	{r0,r3,r4,r5,lr}
		bl 		Wait
		pop 	{r0,r3,r4,r5,pc}


ascii_to_morse:

		push 	{lr}						@ saving register

		check_a:
				cmp 	r8, #0x61 @a .-
				bne 	check_b
				bl		append_dot
				bl		append_dash
				b 		end_char

		check_b:
				cmp 	r8, #0x62 @b -...
				bne 	check_c
				bl		append_dash
				bl		append_dot
				bl		append_dot
				bl		append_dot
				b 		end_char

		check_c:
				cmp 	r8, #0x63 @c -.-.
				bne 	check_d
				bl		append_dash
				bl		append_dot
				bl		append_dash
				bl		append_dot
		  		b 		end_char

		check_d:
				cmp 	r8, #0x64 @d -..
				bne 	check_e
				bl		append_dash
				bl		append_dot
				bl		append_dot
				b 		end_char

		check_e:
				cmp 	r8, #0x65 @e .
				bne 	check_f
				bl		append_dot
				b 		end_char

		check_f:
				cmp 	r8, #0x66 @f ..-.
				bne 	check_g
				bl		append_dot
				bl		append_dot
				bl		append_dash
				bl		append_dot
				b 		end_char

		check_g:
				cmp 	r8, #0x67 @g --.
				bne 	check_h
				bl		append_dash
				bl		append_dash
				bl		append_dot
				b 		end_char

		check_h:
				cmp 	r8, #0x68 @h ....
				bne 	check_i
				bl		append_dot
				bl		append_dot
				bl		append_dot
				bl		append_dot
				b 		end_char

		check_i:
				cmp 	r8, #0x69 @i ..
				bne 	check_j
				bl		append_dot
				bl		append_dot
				b 		end_char

		check_j:
				cmp 	r8, #0x6a @j .---
				bne 	check_k
				bl		append_dot
				bl		append_dash
				bl		append_dash
				bl		append_dash
				b 		end_char

		check_k:
				cmp 	r8, #0x6b @k -.-
				bne 	check_l
				bl		append_dash
				bl		append_dot
				bl		append_dash
				b 		end_char

		check_l:
				cmp 	r8, #0x6c @l .-..
				bne 	check_m
				bl		append_dot
				bl		append_dash
				bl		append_dot
				bl		append_dot
				b 		end_char

		check_m:
				cmp 	r8, #0x6d @m --
				bne 	check_n
				bl		append_dash
				bl		append_dash
				b 		end_char

		check_n:
				cmp 	r8, #0x6e @n -.
				bne 	check_o
				bl		append_dash
				bl		append_dot
				b 		end_char

		check_o:
				cmp 	r8, #0x6f @o ---
				bne 	check_p
				bl		append_dash
				bl		append_dash
				bl		append_dash
				b 		end_char

		check_p:
				cmp 	r8, #0x70 @p .--.
				bne 	check_q
				bl		append_dot
				bl		append_dash
				bl		append_dash
				bl		append_dot
				b 		end_char

		check_q:
				cmp 	r8, #0x71 @q --.-
				bne 	check_r
				bl		append_dash
				bl		append_dash
				bl		append_dot
				bl		append_dash
				b 		end_char

		check_r:
				cmp 	r8, #0x72 @r .-.
				bne 	check_s
				bl		append_dot
				bl		append_dash
				bl		append_dot
				b 		end_char

		check_s:
				cmp 	r8, #0x73 @s ...
				bne 	check_t
				bl		append_dot
				bl		append_dot
				bl		append_dot
				b 		end_char


		check_t:
				cmp 	r8, #0x74 @t -
				bne 	check_u
				bl		append_dash
				b 		end_char

		check_u:
				cmp 	r8, #0x75 @u ..-
				bne 	check_v
				bl		append_dot
				bl		append_dot
				bl		append_dash
				b 		end_char

		check_v:
				cmp 	r8, #0x76 @v ...-
				bne 	check_w
				bl		append_dot
				bl		append_dot
				bl		append_dot
				bl		append_dash
				b 		end_char

		check_w:
				cmp 	r8, #0x77 @w .--
				bne 	check_x
				bl		append_dot
				bl		append_dash
				bl		append_dash
				b 		end_char

		check_x:
				cmp 	r8, #0x78 @x -..-
				bne 	check_y
				bl		append_dash
				bl		append_dot
				bl		append_dot
				bl		append_dash
				b 		end_char

		check_y:
				cmp 	r8, #0x79 @y -.--
				bne 	check_z
				bl		append_dash
				bl		append_dot
				bl		append_dash
				bl		append_dash
				b 		end_char

		check_z:
				cmp 	r8, #0x7a @z --..
				bne 	check_co
				bl		append_dash
				bl		append_dash
				bl		append_dot
				bl		append_dot
				b 		end_char

		check_co:
				cmp 	r8, #0x2c @, --..--
				bne 	check_st
				bl		append_dash
				bl		append_dash		  		  
				bl		append_dot
				bl		append_dot
				bl		append_dash
				bl		append_dash
				b 		end_char

		check_st:
				cmp 	r8, #0x2e @. .-.-.-
				bne 	check_ex
				bl		append_dot
				bl		append_dash		  
				bl		append_dot
				bl		append_dash		  
				bl		append_dot
				bl		append_dash
				b 		end_char		  

		check_ex:
				cmp 	r8, #0x21 @! -.-.--
				bne 	check_qu
				bl		append_dash
				bl		append_dot
				bl		append_dash
				bl		append_dot
				bl		append_dash
				bl		append_dash
				b 		end_char

		check_qu:
				cmp 	r8, #0x3f @? ..--..
				bne 	check_1
				bl		append_dot
				bl		append_dot
				bl		append_dash
				bl		append_dash
				bl		append_dot
				bl		append_dot
				b 		end_char

		check_1:
				cmp 	r8, #0x31 @1 .----
				bne 	check_2
				bl		append_dot
				bl		append_dash
				bl		append_dash
				bl		append_dash
				bl		append_dash
				b 		end_char

		check_2:
				cmp 	r8, #0x32 @2 ..---
				bne 	check_3
				bl		append_dot
				bl		append_dot
				bl		append_dash
				bl		append_dash
				bl		append_dash
				b 		end_char

		check_3:
				cmp 	r8, #0x33 @3 ...--
				bne 	check_4
				bl		append_dot
				bl		append_dot
				bl		append_dot
				bl		append_dash
				bl		append_dash
				b 		end_char

		check_4:
				cmp 	r8, #0x34 @4 ....-
				bne 	check_5
				bl		append_dot
				bl		append_dot
				bl		append_dot
				bl		append_dot
				bl		append_dash
				b 		end_char

		check_5:
				cmp 	r8, #0x35 @5 .....
				bne 	check_6
				bl		append_dot
				bl		append_dot
				bl		append_dot
				bl		append_dot
				bl		append_dot
				b 		end_char

		check_6:
				cmp 	r8, #0x36 @6 -....
				bne 	check_7
				bl		append_dash
				bl		append_dot
				bl		append_dot
				bl		append_dot
				bl		append_dot
				b 		end_char

		check_7:
				cmp 	r8, #0x37 @7 --...
				bne 	check_8
				bl		append_dash
				bl		append_dash
				bl		append_dot
				bl		append_dot
				bl		append_dot
				b 		end_char

		check_8:
				cmp 	r8, #0x38 @8 ---..
				bne 	check_9
				bl		append_dash
				bl		append_dash
				bl		append_dash
				bl		append_dot
				bl		append_dot
				b 		end_char

		check_9:
				cmp 	r8, #0x39 @9 ----.
				bne 	check_0
				bl		append_dash
				bl		append_dash
				bl		append_dash
				bl		append_dash
				bl		append_dot
				b 		end_char

		check_0:
				cmp 	r8, #0x30 @0 -----
				bne 	check_sp
				bl		append_dash
				bl		append_dash
				bl		append_dash
				bl		append_dash
				bl		append_dash
				b 		end_char

		check_sp:
				cmp 	r8, #0x20 @' ' space
				bne		end_char
				bl		append_pipe


		end_char:
		  		bl		 append_space

		pop		{pc}   		@ restoring register

append_dot:
		push	{lr}
		ldr		r8, =morse_code		@ address where we want to write to
		ldr 	r9, =morse_len		@ offset
		ldr 	r6, [r9]			@ load value from r9 into r6
		mov 	r10, #0x2e			@ move character . to r10
		strb 	r10, [r8,r6]		@ str r10 to r8 + r6
		add 	r6, r6, #1 			@ increment r6 by 1
		str     r6, [r9] 			@ store r6 to address at r9
		pop    	{pc}
		
append_dash:
		push	{lr}
		ldr   	r8, =morse_code		@ address where we want to write to
		ldr 	r9, =morse_len		@ offset
		ldr 	r6, [r9]			@ load value from r9 into r6
		mov 	r10, #0x2d			@ move character - to r10
		strb 	r10, [r8,r6]		@ ? str r10 to r8 + r6
		add 	r6, r6, #1 			@ increment r6 by 1
		str     r6, [r9] 			@ store r6 to address at r9
		pop		{pc}   

append_space:
		push   	{lr}
		ldr   	r8, =morse_code		@ address where we want to write to
		ldr 	r9, =morse_len		@ offset
		ldr 	r6, [r9]			@ load value from r9 into r6
		mov 	r10, #0x20			@ move character ' ' to r10
		strb 	r10, [r8,r6]		@ ? str r10 to r8 + r6
		add 	r6, r6, #1 			@ increment r6 by 1
		str     r6, [r9] 			@ store r6 to address at r9
		pop    	{pc}   

append_pipe:
		push   	{lr}
		ldr   	r8, =morse_code		@ address where we want to write to
		ldr 	r9, =morse_len		@ offset
		ldr 	r6, [r9]			@ load value from r9 into r6
		mov 	r10, #0x7c			@ move character | to r10
		strb 	r10, [r8,r6]		@ ? str r10 to r8 + r6
		add 	r6, r6, #1 			@ increment r6 by 1
		str     r6, [r9] 			@ store r6 to address at r9
		pop    {pc}   


_exit:
