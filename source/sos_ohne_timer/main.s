@------------------------
@ Read only
@------------------------
.section .data

@------------------------
@ General constants
@------------------------
.global _start

.equ STACK_P, 0x80000
.equ BASE_ADDR, 0x3f200000
.equ CONST_COUNTER, 0xf4240

@------------------------
@ GPIO Register offset Constants
@------------------------
.equ GPFSEL2, 0x08
.equ GPSET0, 0x1c
.equ GPCLR0, 0x28

@------------------------
@ GPIO addressing offset constants
@------------------------
.equ SET_BIT3, 0x08
.equ SET_BIT21, 0x200000

@------------------------
@ Initialize
@------------------------
.section .init

@------------------------
@ Start label
@------------------------
_start:

@------------------------
@ Initialize variable names for registers
@------------------------
const_counter	.req r0
counter			.req r1
base			.req r2
led				.req r3

@------------------------
@ Initialize general register
@------------------------
mov sp, #STACK_P
ldr base,=BASE_ADDR
ldr const_counter,=CONST_COUNTER

@------------------------
@ Set bit 3 in GPFSEL2
@------------------------
ldr led,=SET_BIT3
str led, [base, #GPFSEL2]

@------------------------
@ Set bit 21 in GPSET0
@------------------------
ldr led,=SET_BIT21
str led, [base, #GPSET0]

@------------------------
@ Code
@------------------------
.section .text

@------------------------
@ Infinite loop
@------------------------
ldr led, =SET_BIT21
main_loop:
	bl morse_sos
b main_loop

@------------------------
@ Subroutine for sos
@------------------------
morse_sos:
	push {lr}
	bl morse_s
	bl morse_o
	bl morse_s
	bl word_brk
	pop {pc}

@------------------------
@ Subroutines for letters
@------------------------
morse_s:
	push {lr}
	bl blink_dot
	bl blink_dot
	bl blink_dot
	bl letter_brk
	pop {pc}

morse_o:
	push {lr}
	bl blink_dash
	bl blink_dash
	bl blink_dash
	bl letter_brk
	pop {pc}

@------------------------
@ Subroutines dot and dash
@------------------------
blink_dot:
	push {lr}
	str led, [base, #GPSET0]
	bl delay_dash
	str led, [base, #GPCLR0]
	bl delay_dot
	pop {pc}

blink_dash:
	push {lr}
	str led, [base, #GPSET0]
	bl delay_dot
	str led, [base, #GPCLR0]
	bl delay_dot
	pop {pc}

@------------------------
@ Subroutines for delay
@------------------------
delay_dot:
	push {lr}
	mov counter, #0
	base_delay:
		add counter, counter, #1
		cmp counter, const_counter
		bne base_delay
	pop {pc}

delay_dash:
	push {lr}
	bl delay_dot
	bl letter_brk
	pop {pc}

letter_brk:
	push {lr}
	bl delay_dot
	bl delay_dot
	pop {pc}

word_brk:
	push {lr}
	bl letter_brk
	bl letter_brk
	pop {pc}