.section .init
.globl _start

.equ STACK_P, 0x80000
.equ BASE, 0x3f200000 @Base address
.equ TIMER, 0x3f003004 @Base address
.equ GPFSEL2, 0x08			@FSEL2 register offset 
.equ GPSET0, 0x1c			@GPSET0 register offset
.equ GPCLR0,0x28			@GPCLR0 register offset
.equ SET_BIT3,  0x08		@sets bit three b1000		
.equ SET_BIT21, 0x200000 	@sets bit 21

_start:

@ Branch to the actual main code.
b main

.section .text

main:

@ Set the stack point to 0x8000.
mov sp, #STACK_P

loop$:
bl SetGpioOn

ldr r0,=200000
bl Wait


bl SetGpioOff

ldr r0,=200000
bl Wait

/*
* Loop over this process forevermore
*/
b loop$

GetGpioAddress: 
	ldr r0,=BASE
	mov pc,lr

SetGpioFunction:
	push {lr}
	ldr 	r1,=SET_BIT3
	str 	r1,[r0,#GPFSEL2]
	ldrb	r1,=SET_BIT21
	pop {pc}

SetGpioOn:	
	push {lr} @ +0,1,2,3 und in wait branch ausführen?
	bl GetGpioAddress
	bl SetGpioFunction
	str 	 r1,[r0,#GPSET0] @ turn led on

	pop {pc}

SetGpioOff:	
	push {lr}
	bl GetGpioAddress
	bl SetGpioFunction
	str r1,[r0,#GPCLR0] @ turn led off

	pop {pc}

GetTimeStamp:
	push {lr}
	ldr r0,=TIMER
	ldm r0, {r4,r5}
	pop {pc}

Wait:
	mov r2,r0					@ pass delay in microsends to r2
	push {lr}
	bl GetTimeStamp 			@ put lower 4 bytes of initial TimeStamp into r4
	mov r3,r4					@ pass TimeStamp value to r3

	looptimer$:
		bl GetTimeStamp			@ retrieve new TimeStamp corresponding to elapsed time
		sub r1,r4,r3			@ r1 = newest TimeStamp (retrieved in last line)  - inital TimeStamp
		cmp r1,r2				@ compare r2 (delay time in ms) to r1 (difference of r0(newest timestamp) and r3 (inital timestamp))
								@ set flag according to r1 - r2 

		bls looptimer$			@ repeat loop if r1 - r2 is greater than zero

	pop {pc}
