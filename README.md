#### "bootfiles" Ordner

Enthält die Dateien, die der RPi benötigt, um von der SD Karte lesen zu können.

#### "source" Ordner

Hier liegen die Files, mit denen man ein kernel.img file kompilieren kann. Dieses generierte kernel.img File kann dann mit den Files aus "bootfiles" (bootcode.bin, fixup.dat und start.elf) zusammen auf die SD kopiert werden. Der Raspberry sollte dann den kompilierten Code ausführen.

###### Kompilieren des kernel.img

Im Terminal in den "source" Ordner wechseln. Mit <code>make -f makefile</code> werden drei Files generiert (kernel.elf, kernel.o und kernel.img). Wobei für uns nur das kernel.img wichtig ist.



#### "example" Ordner

Enthält alle Dateien, die nötig sind, um eine LED über GPIO 21 blinken zu lassen.
Einfach den ganzen Inhalt auf die SD, SD in den Raspberry und an den Strom hängen. Zb zum testen der Schaltung.

